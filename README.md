# About

This is a port of [font awesome](http://fontawesome.com) to a `tikz` library.

# Install

## Download

You can download the [latest artifact](https://gitlab.com/adin/tikz-fa/builds/artifacts/master/download?job=build) that should contain a `tikzlibraryfontawesome.code.tex` file, and extract it in your local `texmf` tree. For example,

```bash
mkdir -p ~/texmf/tex/latex
cd ~/texmf/tex/latex
wget https://gitlab.com/adin/tikz-fa/builds/artifacts/master/download?job=build -O artifacts.zip
unzip artifacts.zip -d tikz-fa
rm artifacts.zip
```

## Clone

Clone this repository in your local `texmf` or in some place where `tikz` can find the `tikzlibraries`. For example

```bash
mkdir -p ~/texmf/tex/latex
cd ~/texmf/tex/latex
git clone git@gitlab.com:adin/tikz-fa.git
```

or simply paste the folder and its contents in there. The actual folder inside `texmf` doesn't matter as long as it is in your local tree.

And then [build it](#build-it).

```bash
python3 tikzify.py
```

# Usage

The icons are defined as `pic` and use the same names as the ones publised in the [free icons gallery](https://fontawesome.com/icons?d=gallery&m=free).

You can use them with `\tikz\pic{<far-icon-name>};` in any place in the text. Like

```tex
This will show an ambulance \tikz\pic{far-ambulance};.
```

Or inside a `tikzpicture` environment, with `\pic {<fa-icon-name>}`
```tex
\begin{tikzpicture}
  \pic {fab-twitter};
  \pic at (0,-1) {fas-ambulance};
\end{tikzpicture}
```

The library provides a wrapper to use it directly in your documents like `\tikzfa{<fa-icon-name>}` or the shorter `\fa{<fa-icon-name>}`

```tex
\tikzfa{fab-twitter}
```

A complete example: 
```tex
\documentclass{article}
\usepackage{tikz}
\usetikzlibrary{fontawesome}

\begin{document}
  
  This is a test using \tikz\pic{fas-home}; or the shortcut \tikzfa{fas-home}
  
  \begin{tikzpicture}
    \pic {fab-twitter};
  \end{tikzpicture}
  
  
  \foreach \size in {tiny,scriptsize, footnotesize, small, normalsize, large, Large, LARGE, huge, Huge}
    {{\csname\size\endcsname \size \ \tikz\pic{fas-home};}\par}
\end{document}
```

Note that the construction of the icon names changed since version 5. Now, each icon can be regular (`far`), solid (`fas`), or a brand (`fab`), plus the name of the icon. For more information check the [styles and prefixes](https://fontawesome.com/how-to-use/svg-with-js#styles-and-prefixes).

## More info

Check [ctan](https://www.ctan.org/pkg/pgf?lang=en) for a complete manual on `pgf` and `tikz` and its usage.


## Troubleshooting

If you are using this library in the titles or author, declare them (that is the macros `\title` and `\author`) within the body of the document (that is after `\begin{document}`) to avoid problems with the macros.

# Build it

You can build and update the `tikz` definition by executing the `python` script.

```bash
python3 tikzify.py
```

The code was tested with `python3`.
