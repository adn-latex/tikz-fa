#!/usr/bin/python3

import os
import xml.etree.ElementTree as et
import time
import urllib.request
import logging
# Regex
import re
from argparse import ArgumentParser
# rest api
import requests
from requests.exceptions import HTTPError

parser = ArgumentParser()
# You can use arg parse to just store a couple constants of the log level
parser.add_argument("-v", "--verbose", help="Increase output verbosity",
                    action="store_const", const=logging.DEBUG, default=logging.INFO)
args = parser.parse_args()

# Now you can just set the const from the arg
logging.basicConfig(level=args.verbose)

# Download helper
def downloadFile(url, file):
  # Download the file from `url` and save it locally under `file_name`:
  with urllib.request.urlopen(url) as response, open(file, 'wb') as out_file:
    data = response.read() # a `bytes` object
    out_file.write(data)

## Definitions
svg_file_names = ["fa-brands-400.svg", "fa-regular-400.svg", "fa-solid-900.svg"]
fa_prefix = ["fab", "far", "fas"]

svg_urls = ["https://raw.githubusercontent.com/FortAwesome/Font-Awesome/master/webfonts/" + f for f in svg_file_names]

svg_files = [os.path.basename(svg_url) for svg_url in svg_urls]
output_file = "tikzlibraryfontawesome.code.tex"

# Get the files
logging.info("Downloading files")
for svg_file, svg_url in zip(svg_files, svg_urls):
  downloadFile(svg_url, svg_file)

## Get version
try:
  response = requests.get("https://api.github.com/repos/FortAwesome/Font-Awesome/releases/latest")
  response.raise_for_status()
  jsonResponse = response.json()
  faVersion = jsonResponse["tag_name"]

except HTTPError as err:
  print(f'HTTP error occurred: {err}')
  faVersion = 'unknown'
except Exception as err:
  print(f'Other error occurred: {err}')
  faVersion = 'unknown'

## Code for library
output = f"""
%% Created from fontawsome {faVersion} ({time.strftime("%Y-%m-%d:%H:%M:%S")})

\\usetikzlibrary{{svg.path}}

%% patch relative code
%% https://tex.stackexchange.com/a/417484/7561
\\makeatletter

\\def\\pgf@lib@svg@curveto@rel@smooth{{%%
  \\ifnum\\pgf@lib@svg@count=0\\relax%% nothing read
  \\else%%
  %% Draw curve
  %% Compute first control point
  \\ifx\\pgf@lib@svg@bezier@last\\pgfutil@empty%%
  \\def\\pgf@lib@svg@first@cp{{\\pgfqpoint{{\\pgf@lib@svg@last@x}}{{\\pgf@lib@svg@last@y}}}}
  \\else
  \\def\\pgf@lib@svg@first@cp{{
    \\pgfpointadd
    {{\\pgfqpoint{{\\pgf@lib@svg@last@x}}{{\\pgf@lib@svg@last@y}}}}
    {{\\pgfpointdiff
      {{\\pgf@lib@svg@bezier@last}}
      {{\\pgfqpoint{{\\pgf@lib@svg@last@x}}{{\\pgf@lib@svg@last@y}}}}
    }}
  }}
  \\fi
  \\pgfpathcurveto
  {{\\pgf@lib@svg@first@cp}}
  {{\\pgfpointadd{{\\pgfqpoint{{\\pgf@lib@svg@last@x}}{{\\pgf@lib@svg@last@y}}}}{{\\pgfqpoint{{\\pgf@lib@svg@get@num{{0}}pt}}{{\\pgf@lib@svg@get@num{{1}}pt}}}}}}%%
  {{\\pgfpointadd{{\\pgfqpoint{{\\pgf@lib@svg@last@x}}{{\\pgf@lib@svg@last@y}}}}{{\\pgfqpoint{{\\pgf@lib@svg@get@num{{2}}pt}}{{\\pgf@lib@svg@get@num{{3}}pt}}}}}}%%
  %% Clear quadratic last point and save new last control point:
  \\let\\pgf@lib@svg@quad@last=\\pgfutil@empty%%
  \\pgf@process{{\\pgfpointadd{{\\pgfqpoint{{\\pgf@lib@svg@last@x}}{{\\pgf@lib@svg@last@y}}}}{{\\pgfqpoint{{\\pgf@lib@svg@get@num{{0}}pt}}{{\\pgf@lib@svg@get@num{{1}}pt}}}}}} %%%%%%%%%%%% fixing this line
  \\edef\\pgf@lib@svg@bezier@last{{\\noexpand\\pgfqpoint{{\\the\\pgf@x}}{{\\the\\pgf@y}}}}%%
  %% update
  \\advance\\pgf@lib@svg@last@x by\\pgf@lib@svg@get@num{{2}}pt%%
  \\advance\\pgf@lib@svg@last@y by\\pgf@lib@svg@get@num{{3}}pt%%
  %% Go on
  \\pgf@lib@svg@read@nums{{4}}{{\\pgf@lib@svg@curveto@rel@smooth}}
  \\fi
}}
\\makeatother

%% New definition
\\newcommand{{\\fa}}[1]{{\\tikz\\pic{{#1}};}}
%% Old definition
\\newcommand{{\\tikzfa}}[1]{{\\fa{{#1}};}}

\\tikzset{{
  /tikz/pics/.unknown/.code={{%%
    \\let\\currentname\\pgfkeyscurrentname%%
    \\PackageInfo{{tikz-fontawesome}}{{Loading '\\currentname.tex' glyph file}}%%
    \\input{{\\currentname.tex}}%%
    \\pgfkeysalso{{\\currentname/.try}}%%
  }}%%
}}%%
"""

with open(output_file, "w") as stream:
  stream.write(output)


# Glyph output header
glyph_header = f"""
%% Created from fontawsome {faVersion} ({time.strftime("%Y-%m-%d:%H:%M:%S")})

\\tikzset{{
"""

## Process
for idx, svg_file in enumerate(svg_files):
  logging.info("Processing %s tree: %s" % (idx, svg_file_names[idx]))
  # Get the tree
  tree = et.parse(svg_file)
  
  # Get the name space
  # https://stackoverflow.com/a/37409050/424986
  ns = dict([node for _, node in et.iterparse(svg_file, events=['start-ns'])])
  # replace the default namespace for something we can use (TODO: find a better solution)
  ns['ns'] = ns.pop('')

  # get the root of the tree
  root = tree.getroot()

  # get the units-per-em
  units_per_em = root.find("./ns:defs/ns:font/ns:font-face", namespaces=ns).get('units-per-em')

  # We will parse the svg definitions and create them with each name
  for glyph in root.findall("./ns:defs/ns:font/ns:glyph", namespaces=ns):
    logging.info("  Processing glyph (%s) %s" % (fa_prefix[idx], glyph.get('glyph-name')))
    
    # Clean the svg path, since tikz doesn't handle numbers of the form: "123.456.789" into "123.456 .789"
    reg = re.compile('(\d*\.\d+)(\.\d+)')
    # get svg path
    path = glyph.get('d')

    # apply fix until it doesn't change
    while True:
      fixed,_ = reg.subn(r'\1 \2', path)
      if fixed == path:
        break
      else:
        path = fixed

    # apply again in case there is more than one
    path,_ = reg.subn(r'\1 \2', path)
    glyph_prefix = fa_prefix[idx]
    glyph_icon = glyph.get('glyph-name')

    # Options of the printed string:
    # - prefix defined by fontawesome (to follow the standard)
    # - the name is on the glyph
    # - the d attribute contains the svg definition
    # Note: check the em conversion rate on a units-per-em https://github.com/FortAwesome/Font-Awesome/tree/master/web-fonts-with-css/webfonts 
    output = glyph_header
    output += f"  {glyph_prefix}-{glyph_icon}/.pic = {{\\fill[scale=1em/{units_per_em}] svg {{{path}}};}}\n}}"

    # Write each glyph individually
    output_file = f"{glyph_prefix}-{glyph_icon}.tex"
    with open(output_file, "w") as stream:
      stream.write(output)

## Clean up
logging.info("Deleting SVG files")
[os.remove(svg_file) for svg_file  in svg_files]


